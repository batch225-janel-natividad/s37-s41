const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");
const dotenv = require("dotenv").config();

//const Routes
const userRoute = require("./routes/userRoute");
const courseRoute = require("./routes/courseRoute");

// Connect Express
const app = express();
const port = 3001;
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended:true}));

//Connect Mongoose

mongoose.connect(`mongodb+srv://janel:${process.env.PASSWORD}@cluster0.yxm0cbf.mongodb.net/course-booking-api?retryWrites=true&w=majority`,
    {
        useNewUrlParser: true,
        useUnifiedTopology: true
    }
);

let db = mongoose.connection

db.on("error", console.error.bind(console, "connection error"));
db.on("open", () => console.log("Connected to MongoDB."));

app.use("/users", userRoute);
app.use("/courses", courseRoute);

app.listen(port, () => console.log(`API is now online on port ${port}`));

/*
/ CORS (Cross-Origin Resource Sharing) is a security feature implemented by web browsers that blocks web pages from making requests to a different domain than the one that served the web page. This is done to prevent malicious websites from stealing sensitive information from other websites\
In Node.js, CORS can be implemented using a middleware package such as cors. It allows you to configure the server to allow or disallow specific origins and HTTP methods. This can be useful if you are building a web application that makes API calls to a server running on a different domain.

/*
	Nodemon is a utility that automatically restarts a Node.js application when a change is made to the code. It is particularly useful when working with Express.js, a popular Node.js framework for building web applications, because it allows developers to make changes to their code and see the results immediately without having to manually stop and restart the server.

	When you use nodemon with Express.js, you can run your application with the command "nodemon app.js" instead of "node app.js". This will start your Express.js application and watch for changes in your code. If you make any changes to your code, nodemon will automatically restart the server and apply the changes, so you can see the updates in your application without having to manually stop and start the server. This can save a lot of time and effort during the development process.

	To install nodemon in an Express.js application, you will first need to have Node.js and npm (Node Package Manager) installed on your computer. If you don't have these already, you can download them from the official Node.js website.

	Once you have Node.js and npm installed, you can install nodemon in your Express.js application by running the following command in your terminal or command prompt:

	npm install -g nodemon
	This will install nodemon globally on your computer, making it available to use in any Node.js project.

	After installing nodemon, you can run your Express.js application using nodemon by running the command:

	nodemon app.js
	in the root of your project, where "app.js" is the entry point to your application.

	Note: the -g flag is used to install nodemon globally, if you don't want to install it globally, just remove the -g flag.

*/
