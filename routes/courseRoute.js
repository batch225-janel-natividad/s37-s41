const express = require("express");
const router = express.Router();
const auth = require("../auth");

const courseController = require("../controllers/courseController");

//Router for creating a course
router.post("/addCourse", auth.verify, (req,res) => {

    const data = {
        course: req.body,
        isAdmin: auth.decode(req.headers.authorization).isAdmin
    }
    courseController.addCourse(data).then(result => res.send(result));
});

//Router for Get course
router.get("/getCourse",  auth.verify, (req,res) => {

    const data = {
        course: req.body,
        isAdmin: auth.decode(req.headers.authorization).isAdmin
    }
    courseController.getCourse(data).then(result => res.send(result));
});

//Route for updating the course
router.put("/:courseId", auth.verify, (req, res) =>{
    courseController.updateCourse(req.params, req.body).then(result => res.send(result));
})

// Route for archive (Using params)
router.put("/archieve/:courseId", auth.verify, (req, res) => {
    courseController.archiveCourse(req.params).then(result => res.send(result));
});
router.put("/unarchieve/:courseId", auth.verify, (req, res) => {
    courseController.unarchiveCourse(req.params).then(result => res.send(result));
});

//Router for users in getting courses

router.get("/getAllActive", auth.verify, (req, res) => {
    courseController.getAllActive().then(result => res.send(result));
})



module.exports = router;


