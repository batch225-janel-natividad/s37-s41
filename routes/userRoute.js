const express = require("express");
const router = express.Router();
const auth = require("../auth");

const userController = require("../controllers/userController");

//Check Email 
router.post("/checkEmail", (req, res) => {
    userController.checkEmailExists(req.body).then(result => res.send(result));
});

//Route for Registration
router.post("/register", (req, res) => {
    userController.registerUser(req.body).then(result => res.send(result))
});

//Route for user.authetication
router.post("/login", (req, res) => {
    userController.loginUser(req.body).then(result => res.send(result));
});

//Route for details
router.post("/details", (req, res) => {
    userController.getProfile(req.body).then(result => res.send(result))
});

//Route for enrolling an authenticated user
router.post('/enroll', auth.verify, (req, res) => {
    let data = {
        // user ID will be retrieved from the request header
        userId : auth.decode(req.headers.authorization).id,
        //Course ID will be retrieve from  the request body
        courseId : req.body.courseId
    }
     userController.enroll(data).then(result => res.send(result))
})

module.exports = router;


