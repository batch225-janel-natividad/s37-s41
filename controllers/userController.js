const User = require("../models/user");
const Course = require("../models/course");
const bcrypt = require("bcrypt");
const auth = require("../auth");

module.exports.checkEmailExists = (reqBody) => {

    return User.find({email : reqBody.email}).then(result => {

        // the find method returns a record if a match is found

        if (result.length > 1) {

            return {
                message: "User was found"
            } 

        } else if (result.length > 0){

            return {
                message: "Duplicate User"
            }

        } else {

            return {
                message: "User not found"
            }
        }


    })
}

module.exports.registerUser = (reqBody) => {
    let newUser = new User({
        firstName : reqBody.firstName,
        lastName : reqBody.lastName,
        email : reqBody.email,
        mobileNo : reqBody.mobileNo,
        password : bcrypt.hashSync(reqBody.password, 10)
        })

    return newUser.save().then((user, error) => {
        if (error) {
            return false;
        } else {
            return user
        };
    });
};

//User Authentication
    /*
        Steps:
        1. Check the dayabase if the user email exists
        2. Compare the passwrod provided in the login from with the password stored in the database
        3. Generate/return a JSON web token if the user is successful logged in and return false if not
        4.
     */

module.exports.loginUser = (reqBody) => {

    return User.findOne({email: reqBody.email}).then(result => {
        
        if(result == null ){
        
            return {
                message : "Not found in our database"
            }
            } else {
        
                    // The "compareSync" method is used to compare a non encrypted password from the login form to the encrypted password retrieved from the database and returns "true" or "false" value depending on the result
        
            const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);
        
        if (isPasswordCorrect){
            return {access : auth.createAccessToken(result)}
            } else {
                return {
                        message : "password incorrect"
                }
                    };
                };
            });
        };

module.exports.getProfile = (userId) => {
    return User.findOne({_id : userId}).then(result => {
        if(!result) return  {message : 'User not found'}
        return result
        delete result.password;
    });
};

module.exports.enroll = async (data) => {
    let isUserUpdated = await User.findById(data.userId).then(user => {

        user.enrollements.push({courseId : data.courseId});

        return user.save().then((user, error) => {
            if (error) {
                return false;
            } else {
                return {
                    message: "You are enrolled!"
                }
            }
        });
    });
    // Add the user ID in the enrollees array of the course
    // Using the "await" keyword will allow the enroll method to complete updating the course before returning a response back to the frontend
    let isCourseUpdated = await Course.findById(data.courseId).then(course => {

        // Adds the userId in the course's enrollees array
        course.enrollees.push({userId : data.userId});
        // Saves the updated course information in the database
        return course.save().then((course, error) => {
            if (error) {
                return false;
            } else {
                return true;
            };
        });

    });
// Condition that will check if the user and course documents have been updated
    // User enrollment successful
    if(isUserUpdated && isCourseUpdated){
        return {
            message: "You are enrolled!"
        }
    // User enrollment failure
    } else {
        return false;
    };
    
}

