const Course = require("../models/course");

module.exports.addCourse = (data) => {
	if(data.isAdmin){
		let new_course = new Course ({
			name: data.course.name,
			description: data.course.description,
			price: data.course.price
		})

		return new_course.save().then((new_course, error) => {
			if(error){
				return false
			}

			return {
				message: 'New course successfully created!'
			}
		})
	} 

	let message = Promise.resolve('User must me Admin to Access this.')

	return message.then((value) => {
		return value

	// you can use this to simplify code
	// Promise.resolve('User must me Admin to Access this.')
	})
		
}
module.exports.getCourse = async (courseId) => {

	const result = await Course.find({});
    return result;
}

module.exports.updateCourse = (reqParams, reqBody) => {

	//Specify the fields/properties of the documents to be updated

	let updatedCourse = {
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	};

		/*
		 findByIdAndUpdate(document ID, updatesToBeApplied)
		 */
		return Course.findByIdAndUpdate(reqParams.courseId, updatedCourse).then((course,error) => {
			if (error) {
				return false;
			} else {
				return {
					message: "Course updated successfully"
				}
			};
		});
};

module.exports.archiveCourse = (reqParams) => {
	
	let updateActiveField = {
		isActive: false,

	};

	return Course.findByIdAndUpdate(reqParams.courseId, updateActiveField).then((course, error) => {
		if (error) {
			return false;
		} else {
			return {
				message: "archiving course successfully"
			};
		};
	});
};

module.exports.unarchiveCourse = (reqParams) => {
	
	let updateActiveField = {
		isActive: true,

	};

	return Course.findByIdAndUpdate(reqParams.courseId, updateActiveField).then((course, error) => {
		if (error) {
			return false;
		} else {
			return {
				message: "unarchiving course successfully"
			};
		};
	});
};

module.exports.getAllActive = async () => {
	const result = await Course.find({isActive: true});
    return result;
}
